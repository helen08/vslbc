<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Stockist */

$this->title = 'Update Stockist: ' . $model->stockist_id;
$this->params['breadcrumbs'][] = ['label' => 'Stockists', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->stockist_id, 'url' => ['view', 'id' => $model->stockist_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="stockist-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

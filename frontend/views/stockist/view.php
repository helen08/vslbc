<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Stockist */

$this->title = $model->stockist_id;
$this->params['breadcrumbs'][] = ['label' => 'Stockists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stockist-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->stockist_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->stockist_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'stockist_id',
            'stockist_name',
            'stockist_email:email',
            'id',
            'agent_loc',
            'description',
        ],
    ]) ?>

</div>

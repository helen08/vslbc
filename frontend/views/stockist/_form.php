<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\Stockist */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="stockist-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'stockist_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'stockist_email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'agent_loc')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Stockist */

$this->title = 'Create Stockist';
$this->params['breadcrumbs'][] = ['label' => 'Stockists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stockist-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "stock".
 *
 * @property int $item_id
 * @property string $item_name
 * @property int $total_item
 * @property string $location
 * @property string $description
 */
class Stock extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'stock';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['item_name', 'total_item', 'location', 'description'], 'required'],
            [['total_item'], 'integer'],
            [['item_name'], 'string', 'max' => 30],
            [['location', 'description'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'item_id' => 'Item ID',
            'item_name' => 'Item Name',
            'total_item' => 'Total Item',
            'location' => 'Location',
            'description' => 'Description',
        ];
    }
}

<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Stockist;

/**
 * StockistSearch represents the model behind the search form of `frontend\models\Stockist`.
 */
class StockistSearch extends Stockist
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['stockist_id', 'id'], 'integer'],
            [['stockist_name', 'stockist_email', 'agent_loc', 'description'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Stockist::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'stockist_id' => $this->stockist_id,
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'stockist_name', $this->stockist_name])
            ->andFilterWhere(['like', 'stockist_email', $this->stockist_email])
            ->andFilterWhere(['like', 'agent_loc', $this->agent_loc])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}

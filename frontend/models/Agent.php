<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "agent".
 *
 * @property int $id
 * @property string $agen_name
 * @property string $agen_email
 * @property int $agen_percent
 */
class Agent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'agent';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['agen_name', 'agen_email', 'agen_percent'], 'required'],
            [['agen_percent'], 'integer'],
            [['agen_name', 'agen_email'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'agen_name' => 'Agen Name',
            'agen_email' => 'Agen Email',
            'agen_percent' => 'Agen Percent',
        ];
    }
}

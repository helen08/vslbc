<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "stockist".
 *
 * @property int $stockist_id
 * @property string $stockist_name
 * @property string $stockist_email
 * @property int $id
 * @property string $agent_loc
 * @property string $description
 */
class Stockist extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'stockist';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['stockist_name', 'stockist_email', 'id', 'agent_loc', 'description'], 'required'],
            [['id'], 'integer'],
            [['stockist_name', 'stockist_email'], 'string', 'max' => 100],
            [['agent_loc', 'description'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'stockist_id' => 'Stockist ID',
            'stockist_name' => 'Stockist Name',
            'stockist_email' => 'Stockist Email',
            'id' => 'ID',
            'agent_loc' => 'Agent Loc',
            'description' => 'Description',
        ];
    }
}
